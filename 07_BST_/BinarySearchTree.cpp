#include <compare>
#include <concepts>
#include <ios>
#include <iostream>
#include <memory>
#include <string>
#include <type_traits>
#include <utility>
#include <format>


namespace bst {
namespace details {
template <typename T>
concept HasLeftInsertionProcessingMethod = requires(T obj) {
  obj.ProcessLeftInsertion();
};

template <typename T>
concept HasRightInsertionProcessingMethod = requires(T obj) {
  obj.ProcessRightInsertion();
};

template <typename T>
concept HasAdditionalRepresentation = requires(T obj) {
  { obj.GetAdditionalRepresentation() } -> std::convertible_to<std::string>;
};

template <typename T>
concept TreeNode = requires(T node, T child) {
  { node->GetLeft() } -> std::convertible_to<T>;
  { node->GetRight() } -> std::convertible_to<T>;

  node->SetLeft(child);
  node->SetRight(child);
};

template <TreeNode T>
bool IsLeaf(const T& node) {
  return node->GetLeft() == nullptr && node->GetRight() == nullptr;
}

template <TreeNode T>
bool HasOneChild(const T& node) {
  return node->GetLeft() != nullptr || node->GetRight() != nullptr;
}

template <TreeNode T>
bool IsLeftChild(const T& node, const T& parent) {
  if (parent == nullptr) {
    return true;
  }
  return parent->GetLeft() == node;
}

template <typename T>
void Swap(T& lhs, T& rhs) {
  std::swap(lhs->GetValue(), rhs->GetValue());
}

}  // namespace details

namespace balance {
namespace details {

template <typename NodeType>
struct NullFindBalancer {
 public:
  using NodePtr = std::shared_ptr<NodeType>;
  void BalanceAtFindBegin(const NodePtr& node) {}
  void BalanceAtFindStep(const NodePtr& node) {}
  void BalanceAtFindEnd(const NodePtr& node) {}
};

template <typename NodeType>
struct NullInsertBalancer {
 public:
  using NodePtr = std::shared_ptr<NodeType>;
  void BalanceAtInsertBegin(const NodePtr& node) {}
  void BalanceAtInsertStep(const NodePtr& node) {}
  void BalanceAtInsertEnd(const NodePtr& node) {}
};

template <typename NodeType>
struct NullEraseBalancer {
 public:
  using NodePtr = std::shared_ptr<NodeType>;
  void BalanceAtEraseBegin(const NodePtr& node) {}
  void BalanceAtEraseStep(const NodePtr& node) {}
  void BalanceAtEraseEnd(const NodePtr& node) {}
};

}  // namespace details

struct EmptyType {};

template <typename T>
concept AbstractBalancer = requires(std::remove_reference_t<T> balancer, T::NodePtr node) {
  balancer.BalanceAtFindBegin(node);
  balancer.BalanceAtFindStep(node);
  balancer.BalanceAtFindEnd(node);

  balancer.BalanceAtInsertBegin(node);
  balancer.BalanceAtInsertStep(node);
  balancer.BalanceAtInsertEnd(node);

  balancer.BalanceAtEraseBegin(node);
  balancer.BalanceAtEraseStep(node);
  balancer.BalanceAtEraseEnd(node);
};

template <typename NodeType>
struct NullBalancer :
    public details::NullEraseBalancer<NodeType>,
    public details::NullInsertBalancer<NodeType>,
    public details::NullFindBalancer<NodeType> {
  using NodeMixin = EmptyType;
  using NodePtr = std::shared_ptr<NodeType>;
};
static_assert(
    AbstractBalancer<NullBalancer<EmptyType>>,
    "Null Balancer does not satisfy balancer interface"
);

template <typename NodeType>
struct AVLStacklessBalancer
  : details::NullFindBalancer<NodeType>,
    details::NullEraseBalancer<NodeType> {
  class NodeMixin : public std::enable_shared_from_this<NodeType> {
   public:
    [[nodiscard]] std::size_t GetDepth() const noexcept { return depth_; }

    [[nodiscard]] std::shared_ptr<NodeType> GetParent() const {
      return parent_.expired() ? std::shared_ptr<NodeType>(nullptr) : parent_.lock();
    }
    void SetParent(const std::shared_ptr<NodeType>& node) { parent_ = node; }
  
    void ProcessLeftInsertion() {
      auto left = static_cast<NodeType*>(this)->GetLeft();
      if (left != nullptr) {
        left->parent_ = static_cast<NodeType*>(this)->shared_from_this();
      }
      RecalcDepth();
    }

    void ProcessRightInsertion() {
      auto right = static_cast<NodeType*>(this)->GetRight();
      if (right != nullptr) {
        right->parent_ = static_cast<NodeType*>(this)->shared_from_this();
      }
      RecalcDepth();
    }

    void RecalcDepth() {
      depth_ = std::max(
          GetDepth(static_cast<NodeType*>(this)->GetLeft()),
          GetDepth(static_cast<NodeType*>(this)->GetRight())) + 1;
    }

    std::string GetAdditionalRepresentation() const noexcept {
      return std::format("depth = {}", depth_);
    } 

   private:
    static std::size_t GetDepth(const std::shared_ptr<NodeType>& node) {
      return node == nullptr ? 0 : node->GetDepth();
    } 
    
    std::size_t depth_{1};
    std::weak_ptr<NodeType> parent_ = std::shared_ptr<NodeType>{nullptr};
  };

  using NodePtr = std::shared_ptr<NodeType>;

  void BalanceAtInsertBegin(const NodePtr& node) {}
  void BalanceAtInsertStep(const NodePtr& node) {}

  void BalanceAtInsertEnd(const NodePtr& node) {
    NodePtr current = node;
    int i = 0;
    while (current != nullptr) {
      auto child_left = current->GetLeft();
      auto child_right = current->GetRight();

      auto depth_left = GetDepth(child_left);
      auto depth_right = GetDepth(child_right);

      if (depth_left - 2 == depth_right) {
        PerformRightBalance(current);
      } else if (depth_right - 2 == depth_left) {
        PerformLeftBalance(current);
      }
      current->RecalcDepth();
      current = current->GetParent();
    }
  }
 private:
  static auto GetDepth(const NodePtr& node) {
    return node == nullptr ? 0 : node->GetDepth();
  }

  static void PerformRightBalance(NodePtr& current) {
    auto a = current->GetLeft();
    auto A = a->GetLeft();
    auto B = a->GetRight();

    if (GetDepth(B) > GetDepth(A)) {
      PerformLeftRotation(a);
      PerformRightRotation(current);
    } else {
      PerformRightRotation(current);
    }
  }

  static void PerformLeftBalance(NodePtr& current) {
    auto a = current->GetRight();
    auto B = a->GetLeft();
    auto C = a->GetRight();

    if (GetDepth(B) > GetDepth(C)) {
      PerformRightRotation(a);
      PerformLeftRotation(current);
    } else {
      PerformLeftRotation(current);
    }
  }

  static void PerformLeftRotation(NodePtr& node) {
    auto right = node->GetRight();
    auto A = node->GetLeft();
    auto B = right->GetLeft();
    auto C = right->GetRight();

    bst::details::Swap(node, right);

    right->SetLeft(A);
    right->SetRight(B);
    node->SetLeft(right);
    node->SetRight(C);
  }

  static void PerformRightRotation(NodePtr& node) {
    auto left = node->GetLeft();
    auto A = left->GetLeft();
    auto B = left->GetRight();
    auto C = node->GetRight();

    bst::details::Swap(node, left);
    
    left->SetLeft(B);
    left->SetRight(C);
    node->SetLeft(A);
    node->SetRight(left);
  }
};

// static_assert(
//     AbstractBalancer<AVLStacklessBalancer<EmptyType>>,
//     "AVL stackless balancer does not satisfy balancer interface"
// );


template <typename NodeType>
struct AVLStackfullBalancer : details::NullFindBalancer<NodeType> {

};

// static_assert(
//     AbstractBalancer<AVLStackfullBalancer<EmptyType>>,
//     "AVL stackfull balancer does not satisfy balancer interface"
// );

}  // namespace balance

template <
    std::totally_ordered T,
    template <typename> typename Balancer = balance::NullBalancer>
    requires balance::AbstractBalancer<Balancer<balance::EmptyType>>
struct BinarySearchTree {
 private:
  class Node;
  using BalancerType = Balancer<Node>;
  using NodePtr = std::shared_ptr<Node>;

 public:
  using NodeType = Node;

  BinarySearchTree() = default;

  template <typename... Args>
  decltype(auto) Emplace(Args&&... args) {
    return EmplaceImpl(std::forward<Args>(args)...);
  }

  void Insert(const T& value) { EmplaceImpl(value); }
  void Insert(T& value) { EmplaceImpl(std::move(value)); }

  decltype(auto) Find(const T& value) {
    return FindImpl(value);
  }

  decltype(auto) Erase(const T& value) {
    return EraseImpl(value);
  }

  decltype(auto) Empty() const {
    return root_ == nullptr;
  }

  void Print() const {
    PrintImpl(root_);
  }

 private:
/////////////// INSERT //////////////////
  template <typename... Args>
  void EmplaceImpl(Args&&... args) {
    if (Empty()) {
      ProcessRootInsertion(std::forward<Args>(args)...);
      return;
    }
    ProcessInsertion(std::forward<Args>(args)...);
  }

  template <typename... Args>
  void ProcessRootInsertion(Args&&... args) {
    root_ = GetNewNode(std::forward<Args>(args)...);
    balancer_.BalanceAtInsertBegin(root_);
    balancer_.BalanceAtInsertStep(root_);
    balancer_.BalanceAtInsertEnd(root_);
  }

  template <typename... Args>
  void ProcessInsertion(Args&&... args) {
    auto node = GetNewNode(std::forward<Args>(args)...);
    auto current = root_;

    balancer_.BalanceAtInsertBegin(current);
    InsertionSteps(node, current);

    // if node was inserted, 'current' referes to inserted node
    balancer_.BalanceAtInsertEnd(current);
  }

  void InsertionSteps(NodePtr& node, NodePtr& current) {
    while (true) {
      balancer_.BalanceAtInsertStep(current);
      if (current->GetValue() == node->GetValue()) {
        return;
      }
      if (current->GetValue() < node->GetValue()) {
          ProcessInsertionRightStep(node, current);
      } else {
          ProcessInsertionLeftStep(node, current);
      }
    }
  }

  static void ProcessInsertionLeftStep(NodePtr& node, NodePtr& current) {
    if (current->GetLeft() == nullptr) {
      current->SetLeft(node);
    }
    MoveToLeft(current);
  }

  static void ProcessInsertionRightStep(NodePtr& node, NodePtr& current) {
    if (current->GetRight() == nullptr) {
      current->SetRight(node);
    }
    MoveToRight(current);
  }

/////////////// FIND //////////////////
  bool FindImpl(const T& value) {
    if (Empty()) {
      return false;
    }
    return FindInNonEmptyTree(value);
  }

  bool FindInNonEmptyTree(const T& value) {
    auto current = root_;
    balancer_.BalanceAtFindBegin(current);

    FindSteps(value, current);

    bool found = (current != nullptr);
    if (found) {
      balancer_.BalanceAtFindEnd(current);
    }

    return found;
  }

  void FindSteps(const T& value, NodePtr& current) {
    while (current != nullptr) {
      balancer_.BalanceAtFindStep(current);

      if (current->GetValue() == value) {
        return;
      }

      if (current->GetValue() < value) {
        MoveToRight(current);
      } else {
        MoveToLeft(current);
      }
    }
  }

/////////////// ERASE //////////////////
  void EraseImpl(const T& value) {
    if (Empty()) {
      return;
    }

    auto current = root_;
    balancer_.BalanceAtEraseBegin(current);
  
    auto parent = MoveToErasingValueAndGetParent(value, current);
    EraseValue(current, parent);
  
    balancer_.BalanceAtEraseEnd(parent);
  }

  NodePtr MoveToErasingValueAndGetParent(const T& value, NodePtr& current) {
    NodePtr parent{nullptr};

    while (current != nullptr) {
      balancer_.BalanceAtEraseStep(current);
      if (current->GetValue() == value) {
        return parent;
      }

      parent = current;
      if (current->GetValue() < value) {
        MoveToRight(current);
      } else {
        MoveToLeft(current);
      }
    }

    return parent;
  }

  void EraseValue(NodePtr& current, NodePtr& parent) {
    if (current == nullptr) {
      return;
    }

    if (IsLeaf(current)) {
      ProcessLeafErasing(current, parent);
      return;
    }

    if (HasOneChild(current)) {
      ProcessOneChildNodeErasing(current, parent);
      return;
    }

    ProcessCompleteNodeErasing(current, parent);
  }

  void ProcessLeafErasing(NodePtr& leaf, NodePtr& parent) {
    if (leaf == root_) {
      EraseRootWithoutChildren();
      return;
    }

    SubstituteNodeForAChild(nullptr, leaf, parent);
  }

  void EraseRootWithoutChildren() { root_ = nullptr; }

  void ProcessOneChildNodeErasing(NodePtr& current, NodePtr& parent) {
    if (current == root_) {
      EraseRootWithOneChild();
      return;
    }
    auto child = GetExistingChild(current);
    SubstituteNodeForAChild(child, current, parent);
  }

  void EraseRootWithOneChild() {
    root_ = GetExistingChild(root_);
  }

  void ProcessCompleteNodeErasing(NodePtr& current, NodePtr& parent) {
    auto child = current->GetLeft();
    balancer_.BalanceAtEraseStep(child);
    parent = current;

    MoveNodeAndItsParentToTheMostRightChild(child, parent);
    SwapNodes(child, current);

    if (IsLeaf(child)) {
      ProcessLeafErasing(child, parent);
      return;
    }
    ProcessOneChildNodeErasing(child, parent);
  }

  void MoveNodeAndItsParentToTheMostRightChild(NodePtr& node, NodePtr& parent) {
    while (node->GetRight() != nullptr) {
      parent = node;
      MoveToRight(node);
      balancer_.BalanceAtEraseStep(node);
    }
  }

/////////////// UTILS //////////////////
  void PrintImpl(NodePtr node) const {
    if (node == nullptr) {
      return;
    }
    std::cout << node->GetStringRepresentation() << '\n';

    PrintImpl(node->GetLeft());
    PrintImpl(node->GetRight());
  }

  static bool IsLeaf(const NodePtr& node) {
    return node->GetLeft() == nullptr && node->GetRight() == nullptr;
  }

  static bool HasOneChild(const NodePtr& node) {
    return node->GetLeft() == nullptr || node->GetRight() == nullptr;
  }

  static bool IsLeftChild(const NodePtr& child, const NodePtr& parent) {
    return parent->GetLeft() == child;
  }

  template <typename... Args>
  static auto GetNewNode(Args&&... args) {
    return std::make_shared<Node>(std::forward<Args>(args)...);
  }

  static auto GetExistingChild(const NodePtr& node) {
    auto child = node->GetLeft();
    return child != nullptr ? child : node->GetRight(); 
  }

  static auto SwapNodes(NodePtr& left, NodePtr& right) requires std::swappable<T> {
    std::swap(left->GetValue(), right->GetValue());
  }

  static auto MoveToRight(NodePtr& node) {
    node = node->GetRight();
  }

  static auto MoveToLeft(NodePtr& node) {
    node = node->GetLeft();
  }

  static auto SubstituteNodeForAChild(const NodePtr& node, const NodePtr& child, NodePtr& parent) {
    bool is_left_child = IsLeftChild(child, parent);
    if (is_left_child) {
      parent->SetLeft(node);
    } else {
      parent->SetRight(node);
    }
  }

  NodePtr root_{nullptr};
  [[no_unique_address]] BalancerType balancer_;
};


template <std::totally_ordered T, template <typename> typename  Balancer>
requires balance::AbstractBalancer<Balancer<balance::EmptyType>>
class BinarySearchTree<T, Balancer>::Node : public Balancer<Node>::NodeMixin {
 private:
  using NodeBase = Balancer<Node>::NodeMixin;

 public:
  Node(const T& value,
       const std::shared_ptr<Node>& left = nullptr,
       const std::shared_ptr<Node>& right = nullptr)
      : value_(value), left_(left), right_(right) {}

  Node(T&& value,
       const std::shared_ptr<Node>& left = nullptr,
       const std::shared_ptr<Node>& right = nullptr)
      : value_(std::move(value)), left_(left), right_(right) {}

  T& GetValue() { return value_; }
  const T& GetValue() const { return value_; }

  void SetLeft(const std::shared_ptr<Node>& left) {
    left_ = left;
    HandleBalancerDependentLeftInsertionPart();
  }

  void SetRight(const std::shared_ptr<Node>& right) {
    right_ = right;
    HandleBalancerDependentRightInsertionPart();
  }

  auto GetLeft() const { return left_; }
  auto GetRight() const { return right_; }

  std::string GetStringRepresentation() const {
    std::string result = std::to_string(value_);
    result += GetBalancerDependentRepresentation();
    return result;
  }

 private:
  void HandleBalancerDependentLeftInsertionPart() {
    if constexpr (details::HasLeftInsertionProcessingMethod<NodeBase>) {
      NodeBase::ProcessLeftInsertion();
    }
  }

  void HandleBalancerDependentRightInsertionPart() {
    if constexpr (details::HasRightInsertionProcessingMethod<NodeBase>) {
      NodeBase::ProcessRightInsertion();
    }
  }

  std::string GetBalancerDependentRepresentation() const {
    if constexpr (details::HasAdditionalRepresentation<NodeBase>) {
      return std::string(" ") + NodeBase::GetAdditionalRepresentation();
    }
    return "";
  }

  T value_;
  std::shared_ptr<Node> left_;
  std::shared_ptr<Node> right_;
};

template <typename T>
using NotBalancedTree = BinarySearchTree<T, balance::NullBalancer>;

template <typename T>
using AVLTree = BinarySearchTree<T, balance::AVLStacklessBalancer>;
}  // namespace bst


int main() {
  bst::AVLTree<int> tree;

  for (int i = 0; i < 20; ++i) {
    tree.Insert(i);
  }

  tree.Print();
}