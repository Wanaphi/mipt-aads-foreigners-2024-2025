#include <iostream>
#include <iterator>
#include <ranges>

int main() {
  std::size_t size;
  std::cin >> size;
  std::ranges::copy(
      std::ranges::istream_view<int>(std::cin) | std::views::transform(
          [](auto x){
            static decltype(x) sum = {};
            sum += x;
            return sum;
          }
      ) | std::views::take(size), std::ostream_iterator<int>(std::cout));
}
