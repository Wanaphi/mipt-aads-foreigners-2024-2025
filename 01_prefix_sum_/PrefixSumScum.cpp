#include <iostream>
#include <numeric>
#include <vector>

int main() {
  std::vector vec = {1, 2, 4, -2, 12};
  std::partial_sum(vec.begin(), vec.end(), vec.begin());
  for (const auto& elem : vec) {
    std::cout << elem << ' ';
  } 
  std::cout << '\n';
}
