#include <iostream>
#include <vector>

int main() {
  std::size_t count;
  std::vector<int> prefix;
  std::cin >> count;
  prefix.reserve(count);
  for (std::size_t i = 0; i < count; ++i) {
    int value = 0;
    std::cin >> value;
    prefix.push_back(i == 0 ? value : value + prefix.back());
  }
  for (const auto& elem : prefix) {
    std::cout << elem << ' ';
  }

}
