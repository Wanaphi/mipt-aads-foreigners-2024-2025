#include <bits/iterator_concepts.h>
#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <array>



template <typename T>
concept RandomAccessContainer = requires (T t) {
  typename T::iterator;
  requires ( std::random_access_iterator<typename T::iterator> ); 
};

static_assert(RandomAccessContainer<std::vector<int>>);
static_assert(RandomAccessContainer<std::deque<int>>);
static_assert(RandomAccessContainer<std::array<int, 100>>);
static_assert(not RandomAccessContainer<std::list<int>>);


template <typename T, RandomAccessContainer Container = std::vector<T>>
class PrefixSumArray : public Container {
  using Base = Container;
 public:
  PrefixSumArray() = default;

  void push_back(const T& value) {
    Base::push_back(Base::empty() ? value : value + Base::back());
  }


  void push_back(T&& value) {
    Base::push_back(Base::empty() ? std::move(value) : value + Base::back());
  }

  T GetRangeSum(std::size_t begin, std::size_t end) const {
    T result = (*this)[end];
    if (begin != 0) {
      result -= (*this)[begin - 1];
    }
    return result;
  }
};



int main() {
  PrefixSumArray<int32_t> prefix;
  prefix.push_back(10);
  prefix.push_back(2);
  prefix.push_back(3);
  prefix.push_back(5);
  prefix.push_back(12);
  prefix.push_back(43);
  std::cout << prefix.GetRangeSum(1, 3) << '\n';
  std::cout << prefix[2] << '\n';
}
