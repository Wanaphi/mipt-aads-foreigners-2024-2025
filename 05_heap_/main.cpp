#include <concepts>
#include <iostream>
#include <vector>

template <typename T, typename Compare = std::less<T>>
requires requires (T t, Compare cmp) {
  { cmp(t, t) } -> std::convertible_to<bool>;
}
class Heap {
 public:
  void Insert(const T& value) {
    data_.push_back(value);
    SiftUp(data_.size() - 1);
  }

  const T& Top() const {
    return data_.front();
  }

  void ExtractTop() {
    std::swap(data_.front(), data_.back());
    data_.pop_back();
    if (Size() > 0) {
      SiftDown(0);
    }
  }

  std::size_t Size() const { return data_.size(); }

 private:
  void SiftUp(std::size_t index) {
    while (index != 0) {
      std::size_t parent = (index - 1) / 2;
      if (compare_(data_[parent], data_[index])) {
        return;
      }
      std::swap(data_[parent], data_[index]);
      index = parent;
    }
  }

  void SiftDown(std::size_t index) {
    while (index < Size() / 2) {
      std::size_t lci = index * 2 + 1;
      if (lci >= Size()) {
        return;
      }
      std::size_t rci = index * 2 + 2;
      std::size_t min_child_index = (rci < Size() && compare_(data_[rci], data_[lci])) ? rci : lci;
      if (compare_(data_[index], data_[min_child_index])) {
        return;
      }
      std::swap(data_[index], data_[min_child_index]);
      index = min_child_index;
    }
  }

  Compare compare_{};
  std::vector<T> data_;
};

template <typename T>
class MeadianHolder {
 public:
  void Insert(const T& value) {
    if (left_.Size() == 0 || value <= GetMedian()) {
      left_.Insert(value);
    } else {
      right_.Insert(value);
    }
    Balance();
  }

  const T& GetMedian() const {
    return left_.Top();
  }

  void ExtractMedian() {
    left_.ExtractTop();
    Balance();
  }

 private:
  void Balance() {
    if (right_.Size() >= left_.Size() + 1) {
      left_.Insert(right_.Top());
      right_.ExtractTop();
      return;
    }
  }

  Heap<T, std::greater<T>> left_;
  Heap<T, std::less<T>> right_;
};


int main() {
  MeadianHolder<int> mh;
  mh.Insert(9);
  mh.Insert(13);
  mh.Insert(6);
  mh.Insert(72);
  mh.Insert(90);
  mh.Insert(60);
  mh.Insert(5);
  mh.Insert(15);
  mh.Insert(4);
  mh.Insert(19);
  
  std::cout << mh.GetMedian() << '\n'; mh.ExtractMedian();
  std::cout << mh.GetMedian() << '\n'; mh.ExtractMedian();
  std::cout << mh.GetMedian() << '\n'; mh.ExtractMedian();
  std::cout << mh.GetMedian() << '\n'; mh.ExtractMedian();
  std::cout << mh.GetMedian() << '\n'; mh.ExtractMedian();
  std::cout << mh.GetMedian() << '\n'; mh.ExtractMedian();
}
