#include <iostream>
#include <vector>
#include <optional>

template <typename T>
class MinStack {
 public:
  MinStack() = default;

  void Push(const T& value) {
    data_.push_back(value);
    if (mins_.empty()) {
      mins_.push_back(value);
    } else {
      mins_.push_back(std::min(value, mins_.back()));
    }
  }

  void Pop() {
    data_.pop_back();
    mins_.pop_back();
  }

  bool Empty() const {
    return data_.empty();
  }

  std::size_t Size() const {
    return data_.size();
  }

  std::optional<T> GetMin() const {
    if (Empty()) {
      return std::nullopt;
    }
    return mins_.back();
  }

  std::optional<T> GetTop() const {
    if (Empty()) {
      return std::nullopt;
    }
    return data_.back();
  }

 private:
  std::vector<T> data_;
  std::vector<T> mins_;
};

template <typename T>
class MinQueue {
 public:
  MinQueue() = default;
  
  void Push(const T& value) {
    stack_in_.Push(value);
  }

  void Pop() {
    if (stack_out_.Empty()) {
      Shift();
    }
    stack_out_.Pop();
  }

  std::optional<T> GetMin() const {
    auto left_min = stack_in_.GetMin();
    auto right_min = stack_out_.GetMin();
    if (!left_min.has_value()) {
      return right_min;
    }
    if (!right_min.has_value()) {
      return left_min;
    }
    return std::min(left_min.value(), right_min.value());
  }

 private:
  void Shift() {
    while (!stack_in_.Empty()) {
      stack_out_.Push(stack_in_.GetTop().value());
      stack_in_.Pop();
    }
  }

  MinStack<T> stack_in_;
  MinStack<T> stack_out_;
};

std::vector<int> GetInputData() {
  std::size_t count;
  std::cin >> count;
  std::vector<int> data(count);
  for (auto& elem : data) {
    std::cin >> elem;
  }
  return data;
}

auto GetBorders() {
  std::size_t left_border;
  std::size_t right_border;
  std::cin >> left_border >> right_border;
  return std::make_pair(left_border, right_border);
}

auto GetPrefixSum(std::vector<int> data) {
  std::vector<int> prefix(data.size());
  prefix[0] = data[0];
  for (std::size_t i = 1; i < prefix.size(); ++i) {
    prefix[i] = prefix[i - 1] + data[i];
  }
  return prefix;
}

int main() {
  auto data = GetInputData();
  auto [left_border, right_border] = GetBorders();
  auto prefix = GetPrefixSum(data);
  
  MinQueue<int> window;
  std::size_t window_size = right_border - left_border + 1;

  for (std::size_t i = 0; i < window_size; ++i) {
    window.Push(0);
  }

  std::size_t j = 0;
  for (std::size_t i = left_border - 1; i < data.size(); ++i) {
    std::cout << (prefix[i] - window.GetMin().value()) << '\n';
    window.Pop();
    window.Push(prefix[j]);
    ++j;
  }
}
